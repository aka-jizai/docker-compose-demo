const Sequelize = require("sequelize");
const sequelize = require("../config/db");

const UserModel = sequelize.define(
  "user",
  {
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    first_name: {
      type: Sequelize.STRING(255),
      allowNull: false,
    },
    last_name: {
      type: Sequelize.STRING(255),
      allowNull: true,
    },
    email_address: {
      type: Sequelize.STRING(255),
      allowNull: false,
    },
    phone_no: {
      type: Sequelize.STRING(255),
      allowNull: false,
    },
    con_message: {
      type: Sequelize.STRING(500),
      allowNull: false,
    },
    createtime: {
      type: Sequelize.DATE,
      allowNull: false,
    }
  },
  {
    timestamps: false,
  }
);

// 反向生成表
sequelize.sync()

module.exports = UserModel;

const Sequelize = require("sequelize");
const sequelize = require("../config/db");
const UserModel = require("../models/UserModel.js");
const router = require("koa-router")();

router.post("/api/concat", async (ctx, next) => {
  const { first_name, last_name, email_address, phone_no, con_message } =
    ctx.request.body;
  // 校验每个字段都要必填
  if (!first_name || !email_address || !phone_no || !con_message) {
    return (ctx.body = {
      code: 1,
      message: "缺少必填字段",
    });
  }
  const user = {
    first_name,
    last_name,
    email_address,
    phone_no,
    con_message,
    createtime: new Date(),
  };
  await UserModel.create(user);
  ctx.body = {
    code: 0,
    message: "success",
  };
});

router.get("/api/db", async (ctx) => {
  const res = await sequelize.query("SHOW TABLES;", {
    type: Sequelize.QueryTypes.SHOWTABLES,
  });
  ctx.body = res;
});
router.get("/api/user", async (ctx) => {
  const res = await UserModel.findOne({
    where: { id: ctx.request.query.id },
  });
  ctx.body = res;
});

module.exports = router;

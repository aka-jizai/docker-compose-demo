const Sequelize = require("sequelize");

const MYSQL_HOST = process.env.MYSQL_HOST;
const MYSQL_DATABASE = process.env.MYSQL_DATABASE;
const MYSQL_PORT = process.env.MYSQL_PORT;
const MYSQL_ROOT_PASSWORD = process.env.MYSQL_ROOT_PASSWORD;

console.log('--- 连接数据库 ---', MYSQL_HOST, MYSQL_DATABASE, MYSQL_PORT);

const sequelize = new Sequelize(MYSQL_DATABASE, "root", MYSQL_ROOT_PASSWORD, {
  host: MYSQL_HOST,
  port: MYSQL_PORT,
  dialect: "mysql",
  charset: "utf8mb4", // 设置默认字符集
  collate: "utf8mb4_unicode_ci", // 设置默认校对规则
});

module.exports = sequelize;
